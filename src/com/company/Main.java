package com.company;

import java.util.Scanner;

import static com.company.selection.selectionSort;

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int pilihan, inGamePlayer=0,random, kill, arrest=0, start=0;
        String [] joblist = {"Magician", "Wolf", "Rakyat","Frakenstain"};
        player [] pemain = new player[4];
        for (int i=0;i<pemain.length;i++){
            pemain[i] = new player();
        }

        do{
            random = ((int) (Math.floor(Math.random() * 4)));
            System.out.println("Game Werewolf ----> 6706174071\n" +
                    "1. Tambah Pemain ["+inGamePlayer+"]\n" +
                    "2. Mulai Permainan");
            System.out.print("Pilihan : ");
            pilihan = scan.nextInt();
            if(pilihan == 1){
                if (inGamePlayer == 4){
                    System.out.println("Pemain Sudah Maksimal");
                    break;
                }
                else {
                    scan.nextLine();
                    System.out.print("Masukkan Nama : ");
                    pemain[inGamePlayer].nama = scan.nextLine();
                    pemain[inGamePlayer].job = joblist[random];
                    pemain[inGamePlayer].life = 1;
                    inGamePlayer += 1;
                    System.out.println("Pemain berhasil ditambahkan");
                }
            }
                if (pilihan == 2){
                    if (inGamePlayer<4){
                        System.out.println("Maaf jumlah pemain kurang");
                        break;
                    }
                    start=1;
                    do {
                        pemain = selectionSort(pemain);
                        System.out.println("Kamu adalah Raja, pilih siapa wolfnya");
                        for (int i=0;i<pemain.length;i++){
                            if (pemain[i].life == 0){
                                System.out.println((i+1) + ". " + "Mati");
                            }
                            else {
                                System.out.println((i+1) + ". " + pemain[i].nama);
                            }
                        }
                        System.out.print("Pilih yang akan dieksekusi : ");
                        kill = scan.nextInt();
                        pemain[kill - 1].life = 0;
                        System.out.println("Berhasil mengeksekusi " +pemain[kill - 1].nama + " dia adalah "+pemain[kill - 1].job);
                        if (pemain[kill - 1].job == "Wolf"){
                            arrest = 1;
                            System.out.println("Selamat!! You Win...");
                        }
                    }while (arrest == 0);
                    break;
                }
        }while (start==0);
    }
}