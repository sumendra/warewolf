package com.company;

public class selection {
    static player[] selectionSort(player[]pemain){
        int min, tmpLife;
        String tmp, tmpJob;
        for (int i=0;i<pemain.length-1;i++){
            min = i;
            for (int j=i+1;j<pemain.length;j++){
                if (pemain[j].nama.compareTo(pemain[min].nama)<0){
                    min=j;
                }
            }
            tmp = pemain[i].nama;
            pemain[i].nama = pemain[min].nama;
            pemain[min].nama = tmp;

            pemain[i].job = pemain [min].job;
            tmpJob = pemain[i].job;
            pemain[min].job = tmpJob;

            pemain[i].life = pemain [min].life;
            tmpLife = pemain [i].life;
            pemain[min].life = tmpLife;
        }
        return pemain;
    }
}
